package com.orionhealth.prms.service;

import com.orionhealth.prms.dto.MedicationResponse;
import com.orionhealth.prms.dto.UserMedicationDTO;
import com.orionhealth.prms.dto.patient.MedicationTakenResponse;
import com.orionhealth.prms.dto.patient.PrescribedMedicationDTO;

import java.util.List;

public interface IUserMedicationService {

    public UserMedicationDTO addUserMedication(PrescribedMedicationDTO prescribedMedicationDTO, String username);

    public List<MedicationResponse> getMedicationByUser(String username);

    List<MedicationTakenResponse> getMedicationTakenByUser(String username);
}
