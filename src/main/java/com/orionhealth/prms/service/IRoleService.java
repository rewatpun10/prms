package com.orionhealth.prms.service;

import com.orionhealth.prms.entity.Role;

public interface IRoleService {

    public Role addRole(String role);

    public Role getByRoleName(String role);

}
