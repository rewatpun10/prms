package com.orionhealth.prms.service;

import com.orionhealth.prms.dto.MedicationDTO;
import com.orionhealth.prms.dto.MedicationResponse;

import java.util.List;

public interface IMedicationService {

    public MedicationDTO addMedication(MedicationDTO medicationDTO);

    List<MedicationResponse> getAllMedications();
}
