package com.orionhealth.prms.service.impl;

import com.orionhealth.prms.dto.MedicationDTO;
import com.orionhealth.prms.dto.MedicationResponse;
import com.orionhealth.prms.dto.UserResponse;
import com.orionhealth.prms.entity.Medication;
import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.repository.MedicationRepository;
import com.orionhealth.prms.service.IMedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicationService implements IMedicationService {

    @Autowired
    private MedicationRepository medicationRepository;

    @Override
    public MedicationDTO addMedication(MedicationDTO medicationDTO) {

        Medication medication = new Medication();
        medication.setMedicineName(medicationDTO.getMedicineName());
        medication.setManufacturedBy(medicationDTO.getManufacturedBy());
        medication.setDosage(medicationDTO.getDosage());

        medicationRepository.save(medication);

        return medicationDTO;
    }

    @Override
    public List<MedicationResponse> getAllMedications() {
        List<Medication> medicationList = medicationRepository.getAllMedications();
        List<MedicationResponse> medicationResponseList = new ArrayList<>();
        for (Medication medication : medicationList) {
            MedicationResponse medicationResponse = new MedicationResponse();
            medicationResponse.setId(medication.getId());
            medicationResponse.setMedicineName(medication.getMedicineName());

            medicationResponseList.add(medicationResponse);
        }
        return medicationResponseList;
    }
}
