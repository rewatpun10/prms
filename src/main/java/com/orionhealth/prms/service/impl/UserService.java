package com.orionhealth.prms.service.impl;

import com.orionhealth.prms.dto.UserResponse;
import com.orionhealth.prms.entity.Role;
import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.repository.RoleRepository;
import com.orionhealth.prms.repository.UserRepository;
import com.orionhealth.prms.service.IUserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService implements IUserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;


    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public User findById(long id) {
        return null;
    }

    @Override
    public User findByUsername(String username) {
        return null;
    }

    @Override
    public User saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        HashSet<Role> roles = new HashSet<>();
        Set<Role> userRoles = user.getRoles();
        for (Role role : userRoles) {
            Role r = roleRepository.findRoleByName(role.getRole());
            if (r != null) {
                roles.add(r);
            }
        }
        user.setRoles(roles);
        return userRepository.save(user);
    }

    @Override
    public List<UserResponse> getAllUsers() {
        List<User> usersList = userRepository.getAllUsers();

        List<UserResponse> userResponseList = new ArrayList<>();
        for (User user : usersList) {
            UserResponse userResponse = new UserResponse();
            userResponse.setUsername(user.getUsername());
            userResponse.setId(user.getId());

            userResponseList.add(userResponse);
        }
        return userResponseList;
    }

    @Override
    public List<UserResponse> getAllPatients() {
        Set<String> roles = new HashSet<>();
        roles.add("ROLE_PATIENT");
        List<User> usersList = userRepository.getAllPatients(roles);
        List<UserResponse> userResponseList = new ArrayList<>();
        for (User user : usersList) {
            UserResponse userResponse = new UserResponse();
            userResponse.setUsername(user.getUsername());
            userResponse.setId(user.getId());
            userResponseList.add(userResponse);
        }
        return userResponseList;
    }
}
