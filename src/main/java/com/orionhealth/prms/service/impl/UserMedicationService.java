package com.orionhealth.prms.service.impl;

import com.orionhealth.prms.dto.MedicationDTO;
import com.orionhealth.prms.dto.MedicationResponse;
import com.orionhealth.prms.dto.UserMedicationDTO;
import com.orionhealth.prms.dto.patient.MedicationTakenResponse;
import com.orionhealth.prms.dto.patient.PrescribedMedicationDTO;
import com.orionhealth.prms.entity.Medication;
import com.orionhealth.prms.entity.Prescription;
import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.entity.UserMedication;
import com.orionhealth.prms.repository.MedicationRepository;
import com.orionhealth.prms.repository.PrescriptionRepository;
import com.orionhealth.prms.repository.UserMedicationRepository;
import com.orionhealth.prms.repository.UserRepository;
import com.orionhealth.prms.service.IUserMedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserMedicationService implements IUserMedicationService {

    private UserMedicationRepository userMedicationRepository;
    private UserRepository userRepository;
    private MedicationRepository medicationRepository;
    private PrescriptionRepository prescriptionRepository;


    @Autowired
    public UserMedicationService(UserMedicationRepository userMedicationRepository, UserRepository userRepository, MedicationRepository medicationRepository,PrescriptionRepository prescriptionRepository) {
        this.userMedicationRepository = userMedicationRepository;
        this.userRepository = userRepository;
        this.medicationRepository = medicationRepository;
        this.prescriptionRepository = prescriptionRepository;
    }

    @Override
    public UserMedicationDTO addUserMedication(PrescribedMedicationDTO prescribedMedicationDTO, String username) {

        User user = userRepository.findByUsername(username);
        Medication medication = medicationRepository.findById(prescribedMedicationDTO.getMedicationId()).get();

        UserMedication userMedication = new UserMedication();
        userMedication.setUser(user);
        userMedication.setMedication(medication);
        userMedication.setMedicationTime(new Date());

        userMedicationRepository.save(userMedication);

        UserMedicationDTO userMedicationDTO = new UserMedicationDTO();
        userMedicationDTO.setMedicationId(userMedication.getId());
        userMedicationDTO.setUser(user.getUsername());
        userMedicationDTO.setDateTime(userMedication.getMedicationTime().toString());
        return userMedicationDTO;

    }

    @Override
    public List<MedicationResponse> getMedicationByUser(String username) {
        User user = userRepository.findByUsername(username);
        Prescription prescription = prescriptionRepository.getPrescriptionsByUser(user.getUsername());
        List<Medication> medications = prescription.getMedication();
        List<MedicationResponse> medicationResponses = new ArrayList<>();

        for (Medication med : medications) {
            MedicationResponse medicationResponse = new MedicationResponse();
            medicationResponse.setMedicineName(med.getMedicineName());
            medicationResponse.setId(med.getId());
            medicationResponses.add(medicationResponse);

        }
        return medicationResponses;
    }

    @Override
    public List<MedicationTakenResponse> getMedicationTakenByUser(String username) {
        User user = userRepository.findByUsername(username);
        List<UserMedication> userMedicationTaken = userMedicationRepository.getMedicationTakenByUser(user.getUsername());
        MedicationTakenResponse medicationTakenResponse = new MedicationTakenResponse();

        List<MedicationTakenResponse> medicationTakenResponses = new ArrayList<>();

        for (UserMedication medicationTakenByUser : userMedicationTaken) {
            MedicationTakenResponse medicationResponse = new MedicationTakenResponse();
            medicationResponse.setMedicationName(medicationTakenByUser.getMedication().getMedicineName());
            medicationResponse.setMedicationTime(medicationTakenByUser.getMedicationTime().toString());
            medicationTakenResponses.add(medicationResponse);

        }

        return medicationTakenResponses;

    }
}
