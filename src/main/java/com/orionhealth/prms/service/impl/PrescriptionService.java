package com.orionhealth.prms.service.impl;

import com.orionhealth.prms.dto.PrescriptionDTO;
import com.orionhealth.prms.entity.Medication;
import com.orionhealth.prms.entity.Prescription;
import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.repository.MedicationRepository;
import com.orionhealth.prms.repository.PrescriptionRepository;
import com.orionhealth.prms.repository.UserRepository;
import com.orionhealth.prms.service.IPrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PrescriptionService implements IPrescriptionService {

    @Autowired
    private PrescriptionRepository prescriptionRepository;


    @Autowired
    private UserRepository userRepository;


    @Autowired
    private MedicationRepository medicationRepository;


//    public PerscriptionService(PrescriptionRepository prescriptionRepository, UserRepository userRepository, MedicationRepository medicationRepository) {
//        this.prescriptionRepository = prescriptionRepository;
//        this.userRepository = userRepository;
//        this.medicationRepository = medicationRepository;
//    }

    @Override
    public Prescription addPrescription(PrescriptionDTO prescriptionDTO) {
        User user = userRepository.findByUsername(prescriptionDTO.getUser());

        List<Long> medicationIds = prescriptionDTO.getMedicationId();

        Iterable<Medication> medications = medicationRepository.findAllById(medicationIds);
        List<Medication> medicationList = StreamSupport.stream(medications.spliterator(), false)
                .collect(Collectors.toList());

        Prescription prescription = new Prescription();
        prescription.setMedication(medicationList);
        prescription.setUser(user);

        return prescriptionRepository.save(prescription);
    }
}
