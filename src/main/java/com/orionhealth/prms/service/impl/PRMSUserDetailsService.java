package com.orionhealth.prms.service.impl;

import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.repository.RoleRepository;
import com.orionhealth.prms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class PRMSUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public PRMSUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("Looking for User with username "+ username);
        User user = userRepository.findByUsername(username);
        if(user==null){
            throw new UsernameNotFoundException("User not found with username : "+username);
        }
        return UserPrincipal.build(user);
    }
}
