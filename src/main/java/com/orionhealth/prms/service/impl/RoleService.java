package com.orionhealth.prms.service.impl;

import com.orionhealth.prms.entity.Role;
import com.orionhealth.prms.repository.RoleRepository;
import com.orionhealth.prms.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService implements IRoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }

    @Override
    public Role addRole(String role) {
        return roleRepository.save(new Role(role));

    }

    @Override
    public Role getByRoleName(String role) {
        return roleRepository.findRoleByName(role);
    }
}
