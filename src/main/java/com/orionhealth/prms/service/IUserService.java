package com.orionhealth.prms.service;

import com.orionhealth.prms.dto.UserResponse;
import com.orionhealth.prms.entity.User;

import java.util.List;

public interface IUserService {

    public User findById(long id);

    public User findByUsername(String username);

    public User saveUser(User user);

    public List<UserResponse> getAllUsers();

    public List<UserResponse> getAllPatients();

}
