package com.orionhealth.prms.service;

import com.orionhealth.prms.dto.PrescriptionDTO;
import com.orionhealth.prms.entity.Prescription;

public interface IPrescriptionService {

    public Prescription addPrescription(PrescriptionDTO prescriptionDTO);
}
