package com.orionhealth.prms.repository;

import com.orionhealth.prms.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {

    @Query("select u from User u where u.username = ?1")
    User findByUsername(String username);


    @Query("Select u from User u")
    List<User> getAllUsers();

    @Query("Select u from User u inner join u.roles r where r.role in :roles")
    List<User> getAllPatients(@Param("roles") Set<String> roles);

}
