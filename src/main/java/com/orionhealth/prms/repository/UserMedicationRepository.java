package com.orionhealth.prms.repository;

import com.orionhealth.prms.entity.UserMedication;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserMedicationRepository extends CrudRepository<UserMedication,Long> {

    @Query("select um from UserMedication um where um.user.username = ?1")
    List<UserMedication> getMedicationTakenByUser(String username);
}
