package com.orionhealth.prms.repository;

import com.orionhealth.prms.entity.Medication;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface MedicationRepository extends CrudRepository<Medication,Long> {

    @Query("Select m from Medication m")
    List<Medication> getAllMedications();
}
