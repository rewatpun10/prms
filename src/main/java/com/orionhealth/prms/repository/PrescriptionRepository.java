package com.orionhealth.prms.repository;

import com.orionhealth.prms.entity.Prescription;
import com.orionhealth.prms.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PrescriptionRepository extends CrudRepository<Prescription,Long> {
    @Query("Select p from Prescription p where p.user.username=?1")
    public Prescription getPrescriptionsByUser(String username);
}
