package com.orionhealth.prms.controller.patient;

import com.orionhealth.prms.dto.MedicationDTO;
import com.orionhealth.prms.dto.MedicationResponse;
import com.orionhealth.prms.dto.UserMedicationDTO;
import com.orionhealth.prms.dto.UserRequest;
import com.orionhealth.prms.dto.patient.MedicationTakenResponse;
import com.orionhealth.prms.dto.patient.PrescribedMedicationDTO;
import com.orionhealth.prms.service.IUserMedicationService;
import com.orionhealth.prms.util.JWTTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/patient")
@CrossOrigin
public class UserMedicationController {

    private IUserMedicationService userMedicationService;

    private JWTTokenUtil tokenUtil;

    @Autowired
    public UserMedicationController(IUserMedicationService userMedicationService, JWTTokenUtil tokenUtil) {
        this.userMedicationService = userMedicationService;
        this.tokenUtil = tokenUtil;
    }


    @RequestMapping(value = "/usermed", method = RequestMethod.POST)
    public UserMedicationDTO addUserMedication(@RequestHeader("Authorization") String authString, @RequestBody PrescribedMedicationDTO prescribedMedicationDTO) {
        String jwtToken = authString.split(" ")[1];
        String username = tokenUtil.getUserFromToken(jwtToken);
        return userMedicationService.addUserMedication(prescribedMedicationDTO, username);
    }

    @RequestMapping("/medication")
    public List<MedicationResponse> getMedicationByUser(@RequestHeader("Authorization") String authString) {
        String jwtToken = authString.split(" ")[1];
        String username = tokenUtil.getUserFromToken(jwtToken);
        return userMedicationService.getMedicationByUser(username);
    }

    @RequestMapping("/getMedicationTaken")
    public List<MedicationTakenResponse> getMedicationTakenByUser(@RequestHeader("Authorization") String authString) {
        String jwtToken = authString.split(" ")[1];
        String username = tokenUtil.getUserFromToken(jwtToken);
        return userMedicationService.getMedicationTakenByUser(username);
    }

}
