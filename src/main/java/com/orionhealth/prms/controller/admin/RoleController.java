package com.orionhealth.prms.controller.admin;


import com.orionhealth.prms.entity.Role;
import com.orionhealth.prms.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin
public class RoleController {

    private IRoleService roleService;

    @Autowired
    public RoleController(IRoleService roleService) {
        this.roleService = roleService;
    }

    @RequestMapping("/role")
    public List<String> addRoles(@RequestBody List<String> roles) {

        List<String> roleResp = new ArrayList<>();
        for (String role : roles) {
            Role r = roleService.addRole(role);
            roleResp.add(r.getRole());
        }
        return roleResp;
    }
}
