package com.orionhealth.prms.controller.admin;


import com.orionhealth.prms.dto.MedicationDTO;
import com.orionhealth.prms.dto.MedicationResponse;
import com.orionhealth.prms.dto.UserResponse;
import com.orionhealth.prms.service.IMedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin
public class MedicationController {

    private IMedicationService medicationService;

    @Autowired
    public MedicationController(IMedicationService medicationService){
        this.medicationService = medicationService;
    }

    @RequestMapping("/medication")
    public MedicationDTO addMedication(@RequestBody MedicationDTO medicationDTO){

        return medicationService.addMedication(medicationDTO);
    }

    @RequestMapping(value = "/medications", method = RequestMethod.GET)
    public List<MedicationResponse> getAllMedications() {
        return medicationService.getAllMedications();
    }
}
