package com.orionhealth.prms.controller.admin;


import com.orionhealth.prms.dto.PrescriptionDTO;
import com.orionhealth.prms.entity.Prescription;
import com.orionhealth.prms.service.IPrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin
public class PrescriptionController {

    private IPrescriptionService prescriptionService;
    @Autowired
    public PrescriptionController(IPrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }

    @RequestMapping(value = "/prescription", method = RequestMethod.POST)
    public PrescriptionDTO addPrescription(@RequestBody PrescriptionDTO prescriptionDTO) {

        prescriptionService.addPrescription(prescriptionDTO);

        return prescriptionDTO;
    }
}
