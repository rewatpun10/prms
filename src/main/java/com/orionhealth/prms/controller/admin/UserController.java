package com.orionhealth.prms.controller.admin;


import com.orionhealth.prms.dto.UserRequest;
import com.orionhealth.prms.dto.UserResponse;
import com.orionhealth.prms.entity.Role;
import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.repository.UserRepository;
import com.orionhealth.prms.service.IRoleService;
import com.orionhealth.prms.service.IUserService;
import com.orionhealth.prms.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin
public class UserController {

    private IUserService userService;


    IRoleService roleService;


    @Autowired
    public UserController(IUserService userService, IRoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @RequestMapping(value = "/create-admin",method = RequestMethod.POST)
    public UserResponse createAdminUser(@RequestBody UserRequest userRequest) {
        User user = new User();
        user.setUsername(userRequest.getUsername());
        user.setPassword(userRequest.getPassword());

        Set<Role> userRoles = new HashSet<>();
        userRoles.add(new Role("ADMIN"));
        user.setRoles(userRoles);
        User userEntity = userService.saveUser(user);

        UserResponse userResponse = new UserResponse();
        userResponse.setUsername(userEntity.getUsername());
        userResponse.setId(userEntity.getId());

        return userResponse;
    }


    @RequestMapping(value = "/create-patient",method = RequestMethod.POST)
    public UserResponse createPatientUser(@RequestBody UserRequest userRequest) {
        User user = new User();
        user.setUsername(userRequest.getUsername());
        user.setPassword(userRequest.getPassword());

        Set<Role> userRoles = new HashSet<>();
        Role patientRole = roleService.getByRoleName("ROLE_PATIENT");
        userRoles.add(patientRole);
        user.setRoles(userRoles);
        User userEntity = userService.saveUser(user);

        UserResponse userResponse = new UserResponse();
        userResponse.setUsername(userEntity.getUsername());
        userResponse.setId(userEntity.getId());

        return userResponse;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserResponse> getAllUsers() {
        return userService.getAllUsers();
    }

    @RequestMapping(value = "/patient")
    public List<UserResponse> getAllPatient() {
        return userService.getAllPatients();
    }


}
