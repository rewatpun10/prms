package com.orionhealth.prms.controller;


import com.orionhealth.prms.dto.UserRequest;
import com.orionhealth.prms.dto.UserResponse;
import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/signup")
@CrossOrigin
public class SignUpController {

    private IUserService userService;

    @Autowired
    public SignUpController(IUserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.POST)
    private UserResponse signUp(@RequestParam UserRequest request) {
        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(request.getPassword());
        User userEntity = userService.saveUser(user);

        UserResponse response = new UserResponse();
        response.setUsername(userEntity.getUsername());
        response.setId(userEntity.getId());
        return response;
    }
}
