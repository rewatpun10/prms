package com.orionhealth.prms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PrescriptionDTO {

    @JsonProperty(value = "user",required = true)
    private String user;

    @JsonProperty(value = "medication_id",required = true)
    private List<Long> medicationId;
}
