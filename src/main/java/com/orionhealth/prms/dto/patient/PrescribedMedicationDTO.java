package com.orionhealth.prms.dto.patient;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class PrescribedMedicationDTO {

    @JsonProperty(value = "id",required = true)
    private Long medicationId;
}
