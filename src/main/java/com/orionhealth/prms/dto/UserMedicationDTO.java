package com.orionhealth.prms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserMedicationDTO {

    @JsonProperty(value = "user",required = true)
    private String user;

    @JsonProperty(value = "medication_id",required = true)
    private long medicationId;

    @JsonProperty(value = "date_time",required = true)
    private String dateTime;
}
