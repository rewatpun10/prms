package com.orionhealth.prms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MedicationDTO {

    @JsonProperty(value = "medicine_name",required = true)
    private String medicineName;

    @JsonProperty(value = "manufactured_by",required = true)
    private String manufacturedBy;

    @JsonProperty(value = "dosage",required = false)
    private String dosage;
}
