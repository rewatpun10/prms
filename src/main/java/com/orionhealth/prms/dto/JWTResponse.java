package com.orionhealth.prms.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JWTResponse {

    private final String jwttoken;
    private final String role;
    private final String username;

    public JWTResponse(String jwttoken, String role, String username) {
        this.jwttoken = jwttoken;
        this.role = role;
        this.username = username;
    }
}
