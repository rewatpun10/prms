package com.orionhealth.prms.util;


import com.orionhealth.prms.config.jwt.JWTProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JWTTokenUtil {

    @Autowired
    private JWTProvider tokenProvider;


    public String getUserFromToken(String token){
        return tokenProvider.getUserNameFromJwtToken(token);
    }
}
