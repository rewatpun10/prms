package com.orionhealth.prms;

import com.orionhealth.prms.entity.Role;
import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.service.IRoleService;
import com.orionhealth.prms.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class PrmsApplication {

	@Autowired
	IUserService userService;

	@Autowired
	IRoleService roleService;

	public static void main(String[] args) {
		SpringApplication.run(PrmsApplication.class, args);
	}

	@PostConstruct
	public void init() {
		User user = new User();
		user.setUsername("admin");
		user.setPassword("test");

		Role role  = roleService.addRole("ROLE_ADMIN");
		Role patientRole = roleService.addRole("ROLE_PATIENT");
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		user.setRoles(roles);
		userService.saveUser(user);
	}

}
