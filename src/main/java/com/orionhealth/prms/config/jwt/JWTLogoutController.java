package com.orionhealth.prms.config.jwt;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class JWTLogoutController {

    private JWTProvider jwtProvider;

    public JWTLogoutController(JWTProvider jwtProvider) {
        this.jwtProvider = jwtProvider;
    }


    @RequestMapping("/logout")
    public void logout(@RequestHeader("token") String token) {
        jwtProvider.invalidateToken(token);
    }
}
