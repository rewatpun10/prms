package com.orionhealth.prms.config.jwt;

import com.orionhealth.prms.service.impl.PRMSUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class JWTAuthTokenFilter extends OncePerRequestFilter {

    private JWTProvider tokenProvider;
    private PRMSUserDetailsService prmsUserDetailsService;

    @Autowired
    public JWTAuthTokenFilter(JWTProvider tokenProvider, PRMSUserDetailsService prmsUserDetailsService) {
        this.tokenProvider = tokenProvider;
        this.prmsUserDetailsService = prmsUserDetailsService;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {


        String jwt = getJwt(request);

        if (jwt != null && tokenProvider.validateJwtToken(jwt)) {
            String username = tokenProvider.getUserNameFromJwtToken(jwt);

            UserDetails userDetails = prmsUserDetailsService.loadUserByUsername(username);

            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
                    null, userDetails.getAuthorities());
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        filterChain.doFilter(request, response);

    }


    private String getJwt(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");

        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.replaceFirst("Bearer ", "");
        }

        return null;
    }
}
