package com.orionhealth.prms.config.jwt;

import com.orionhealth.prms.dto.JWTRequest;
import com.orionhealth.prms.dto.JWTResponse;
import com.orionhealth.prms.dto.MedicationResponse;
import com.orionhealth.prms.entity.User;
import com.orionhealth.prms.service.impl.PRMSUserDetailsService;
import com.orionhealth.prms.util.JWTTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin
public class JWTAuthenticationController {

    private AuthenticationManager authenticationManager;
    private PRMSUserDetailsService prmsUserDetailsService;
    private JWTProvider jwtProvider;
    private JWTTokenUtil tokenUtil;


    @Autowired
    public JWTAuthenticationController(AuthenticationManager authenticationManager, PRMSUserDetailsService prmsUserDetailsService, JWTProvider jwtProvider, JWTTokenUtil tokenUtil) {
        this.authenticationManager = authenticationManager;
        this.prmsUserDetailsService = prmsUserDetailsService;
        this.jwtProvider = jwtProvider;
        this.tokenUtil = tokenUtil;
    }

    @RequestMapping(value = "/api/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JWTRequest authenticationRequest) throws Exception {
        Authentication auth = authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = prmsUserDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());
        System.out.println("user request is : " + userDetails.getUsername() + " password: " + userDetails.getPassword() + " authorities: " + userDetails.getAuthorities().toString());
        final String token = jwtProvider.generateJWTToken(auth);
        return ResponseEntity.ok(new JWTResponse(token, userDetails.getAuthorities().toString(), userDetails.getUsername()));
    }

    private Authentication authenticate(String username, String password) throws Exception {
        try {
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }

    }

    @RequestMapping("/auth/signedIn")
    public boolean getMedicationByUser(@RequestHeader("Authorization") String authString) {
        String jwtToken = authString.split(" ")[1];
        String username = tokenUtil.getUserFromToken(jwtToken);
        if(username != null) {
            return true;
        }else {
            return false;
        }
    }

}
