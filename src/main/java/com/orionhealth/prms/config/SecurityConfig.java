package com.orionhealth.prms.config;

import com.orionhealth.prms.config.jwt.JWTAuthEntryPoint;
import com.orionhealth.prms.config.jwt.JWTAuthTokenFilter;
import com.orionhealth.prms.config.jwt.JWTProvider;
import com.orionhealth.prms.service.impl.PRMSUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.CrossOrigin;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private PRMSUserDetailsService prmsUserDetailsService;

    private JWTAuthEntryPoint unauthorizedHandler;

    private JWTProvider tokenProvider;

    @Autowired
    public SecurityConfig(PRMSUserDetailsService prmsUserDetailsService, JWTAuthEntryPoint unautuhorizedHandler,
                          JWTProvider tokenProvider) {
        this.prmsUserDetailsService = prmsUserDetailsService;
        this.unauthorizedHandler = unautuhorizedHandler;
        this.tokenProvider = tokenProvider;
    }

    @Bean
    public JWTAuthTokenFilter authenticationTokenFilter() {
        return new JWTAuthTokenFilter(tokenProvider, prmsUserDetailsService);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(prmsUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/authenticate").permitAll()
                .antMatchers("/api/logout").permitAll()
                .antMatchers("/api/signup").permitAll()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/auth/signedIn").permitAll()
                .antMatchers("/api/admin/**").hasRole("ADMIN")
                .antMatchers("/api/patient/**").hasRole("PATIENT").anyRequest()
                .authenticated().and().exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        // add this line to use H2 web console
        http.headers().frameOptions().disable();
        http.addFilterBefore(authenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
