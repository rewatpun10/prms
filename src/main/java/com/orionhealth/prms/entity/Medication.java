package com.orionhealth.prms.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Medication extends BaseEntity {

    @Column(unique = true)
    private String medicineName;

    private String manufacturedBy;

    private String dosage;

}
