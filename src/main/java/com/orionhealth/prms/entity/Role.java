package com.orionhealth.prms.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.UniqueConstraint;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Role extends BaseEntity {

    @Column(unique = true)
    private String role;

    public Role(String role){
        this.role = role;
    }
}
